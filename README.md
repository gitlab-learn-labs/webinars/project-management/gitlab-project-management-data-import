# Data Seeder from Ruby

## Overview

[Watch the Overview Video](https://youtu.be/4ZMLr8oDhqI)

The goal is to create "pretty demo data" that can be imported into any GitLab self-managed instance that allows SAs to demo GitLab features well, and also allows our customers to see what "good" GitLab data looks like.

We are using the ruby version of [the GitLab data seeder](https://docs.gitlab.com/ee/topics/data_seeder.html), which takes advantage of FactoryBot and the models defined for GitLab to be able to define a set of demo data that gets created directly in the database.  This allows us to create data that cannot be created via the apis, e.g. creation and close dates of issues, with relative dates such as `2.days.ago`.

In tandom, we are working on a [GitLab Seed Generator](https://gitlab.com/gitlab-com/channel/partners/amer/nextlink-labs/collaboration/seed-generator) that allows us to generate a .rb seed file from an existing GitLab group and use that as a starting point for the data seeder file.

The current `demo_group.rb` file contains data for a comprehensive portfolio and project planning demo, including:
- group and project hierarchy to show multiple teams in an Agile program
- use of milestone for midrange planning (called a `Program increment` or `PI` in SAFE framework)
- common iteration cadence defined across the program (group level)
- standard labels for:
    - work item type: initiative, feature, story, bug, improvement
    - work item status: project backlog, in-progress, awaiting acceptance
    - work item priority: high, medium, low
    - initiative theme - delight users, first-class-technology, increase-customer-spend, reduce-tech-debt
- epic hierarchy of initiatives and features to show roadmap view and progress
- epic board to manage initiatives by theme
- project level sprint tracking board to track progress of current iteration

See [Agile portfolio and project planning and tracking demo](https://gitlab.com/gitlab-learn-labs/webinars/project-management/gitlab-project-management-data-import/-/wikis/Agile-portfolio-planning-and-tracking-demo) wiki for a demo script for agile portfolio and project planning and tracking.

We will continue to iterate to build out additional pieces of "pretty demo data" to provide a single end-to-end demo flow.  Next up - SCM and code review workflows, CI/CD, and Security.


[Board for completed and planned  work](https://gitlab.com/gitlab-learn-labs/webinars/project-management/gitlab-project-management-data-import/-/boards)

## How to Run the Data Seeder

The data seeder must be used with a GitLab self-managed instance with an Ultimate license applied.  There are [instructions here](https://gitlab.com/-/snippets/2390362?_gl=1*18nik38*_ga*MTIwODYzMzYxOC4xNjY5OTAwNzYw*_ga_ENFH3X7M5Y*MTY4NTYzNDcxOS41NTguMS4xNjg1NjM0NzgwLjAuMC4w#import-the-test-resources) for how to spin up a GitLab self-managed instance using Docker and then import the test resources and seed the data.   To use the `demo_group.rb` file contained in this repo, you will want to follow the linked instructions, then copy the `demo_group.rb` file:

`sudo docker cp demo_group.rb gitlab:/opt/gitlab/embedded/service/gitlab-rails/ee/db/seeds/data_seeder/demo_group.rb`

Then run the command to seed the data:

`sudo docker exec -it gitlab gitlab-rake "ee:gitlab:seed:data_seeder[demo_group,1]"`

