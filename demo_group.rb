# frozen_string_literal: true

class DataSeeder
  def seed
    @namespace = create(:group, name: 'Agile Program', path: "demo-group-#{owner.username}-#{SecureRandom.hex(3)}")
    @labels = {
      it_support: create(:group_label,
        group: namespace,
        title: 'IT_Support',
        color: '#eee600',
        description: 'IT Support Service Desk Issues'),
      priority_high: create(:group_label,
        group: namespace,
        title: 'Priority::High',
        color: '#ff0000',
        description: 'High priority issues'),
      priority_low: create(:group_label,
        group: namespace,
        title: 'Priority::Low',
        color: '#6699cc',
        description: 'Low priority issues'),
      priority_medium: create(:group_label,
        group: namespace,
        title: 'Priority::Medium',
        color: '#eee600',
        description: 'Medium priority issues'),
      status_awaiting_acceptance: create(:group_label,
        group: namespace,
        title: 'Status::Awaiting_Acceptance',
        color: '#6699cc',
        description: 'for issue and feature workflow, work is done and awaiting PO/PM acceptance'),
      status_in_progress: create(:group_label,
        group: namespace,
        title: 'Status::In-Progress',
        color: '#0000ff',
        description: 'work in progress'),
      status_project_backlog: create(:group_label,
        group: namespace,
        title: 'Status::Project Backlog',
        color: '#0000ff',
        description: 'Ready to be pulled'),
      type_bug: create(:group_label,
        group: namespace,
        title: 'Type::Bug',
        color: '#c21e56',
        description: ''),
      type_feature: create(:group_label,
        group: namespace,
        title: 'Type::Feature',
        color: '#0000ff',
        description: ''),
      type_improvement: create(:group_label,
        group: namespace,
        title: 'Type::Improvement',
        color: '#eee600',
        description: ''),
      type_initiative: create(:group_label,
        group: namespace,
        title: 'Type::Initiative',
        color: '#6699cc',
        description: ''),
      type_story: create(:group_label,
        group: namespace,
        title: 'Type::Story',
        color: '#00b140',
        description: ''),
      infrastructure: create(:group_label,
        group: namespace,
        title: 'infrastructure',
        color: '#eee600',
        description: ''),
      vulnerability: create(:group_label,
        group: namespace,
        title: 'vulnerability',
        color: '#eee600',
        description: ''),
      theme_delight_users: create(:group_label,
        group: namespace,
        title: 'theme::delight-users',
        color: '#8E44AD',
        description: ''),
      theme_first_class_technology: create(:group_label,
        group: namespace,
        title: 'theme::first-class-technology',
        color: '#8E44AD',
        description: ''),
      theme_increase_customer_spend: create(:group_label,
        group: namespace,
        title: 'theme::increase-customer-spend',
        color: '#8E44AD',
        description: ''),
      theme_reduce_tech_debt: create(:group_label,
        group: namespace,
        title: 'theme::reduce-tech-debt',
        color: '#8E44AD',
        description: ''),
    }

    @milestones = {
      PI_1: create(:milestone,
        :on_group,
        group: namespace,
        title: 'Program Increment #1',
        start_date: 8.days.ago,
        due_date: 104.days.from_now),
      PI_2: create(:milestone,
        :on_group,
        group: namespace,
        title: 'Program Increment #2',
        start_date: 105.days.from_now,
        due_date: 217.days.from_now),
    }

    @epics = {
      epic_121088: create(:epic,
        group: namespace,
        title: 'Cloud Native Shopping App',
        author: owner,
        start_date: 1.months.ago,
        due_date: 2.months.from_now,
        labels: [labels[:type_initiative], labels[:theme_delight_users], labels[:status_in_progress]],
        confidential: false,
        state: 'opened',
        created_at: 4.months.ago,
        description: ''),

      epic_121089: create(:epic,
        group: namespace,
        title: 'Implement AI',
        author: owner,
        start_date: 10.days.ago,
        due_date: 4.months.from_now,
        labels: [labels[:status_project_backlog], labels[:type_initiative], labels[:theme_first_class_technology]],
        confidential: false,
        state: 'opened',
        created_at: 2.months.ago,
        description: ''),

      epic_1: create(:epic,
        group: namespace,
        title: 'Improve Shopping Analytics',
        author: owner,
        start_date: 1.month.from_now,
        due_date: 3.months.from_now,
        labels: [labels[:type_initiative], labels[:theme_increase_customer_spend], labels[:status_project_backlog]],
        confidential: false,
        state: 'opened',
        created_at: 4.months.ago,
        description: ''),

      epic_2: create(:epic,
        group: namespace,
        title: 'Enable app server autoscaling on EKS',
        author: owner,
        start_date: 2.months.from_now,
        due_date: 3.months.from_now,
        labels: [labels[:type_initiative], labels[:theme_first_class_technology], labels[:status_project_backlog]],
        confidential: false,
        state: 'opened',
        created_at: 4.months.ago,
        description: ''),

      epic_3: create(:epic,
        group: namespace,
        title: 'Suggest additional items for purchase',
        author: owner,
        start_date: 2.months.from_now,
        due_date: 4.months.from_now,
        labels: [labels[:type_initiative], labels[:theme_increase_customer_spend], labels[:status_project_backlog]],
        confidential: false,
        state: 'opened',
        created_at: 4.months.ago,
        description: ''),
    }

    @level2_epics = {
      epic_693728: create(:epic,
        group: namespace,
        title: 'Integrate EKS Monitoring',
        parent: epics[:epic_121088],
        author: owner,
        start_date: 1.month.from_now,
        due_date: 3.months.from_now,
        labels: [labels[:status_project_backlog], labels[:infrastructure]],
        confidential: false,
        state: 'opened',
        created_at: 2.months.ago,
        description: ''),

      epic_693725: create(:epic,
        group: namespace,
        title: 'Enable app server autoscaling on EKS',
        author: owner,
        parent: epics[:epic_121088],
        start_date: 1.week.from_now,
        due_date: 1.month.from_now,
        labels: [labels[:status_project_backlog], labels[:type_feature], labels[:infrastructure]],
        confidential: false,
        state: 'opened',
        created_at: 3.months.ago,
        description: ''),

      epic_693723: create(:epic,
        group: namespace,
        title: 'Save favorites',
        author: owner,
        parent: epics[:epic_121088],
        start_date: 1.month.ago,
        due_date: 1.week.ago,
        labels: [labels[:type_feature], labels[:status_awaiting_acceptance]],
        confidential: false,
        state: 'closed',
        created_at: 3.months.ago,
        description: ''),

      epic_693711: create(:epic,
        group: namespace,
        title: 'One Click Checkout',
        author: owner,
        parent: epics[:epic_121088],
        start_date: 1.week.ago,
        due_date: 2.weeks.from_now,
        labels: [labels[:type_feature], labels[:status_in_progress]],
        confidential: false,
        state: 'opened',
        created_at: 3.months.ago,
        description: ''),

    }

    @iteration_cadences = {
      cadence1: create(:iterations_cadence, title: "Plan cadence", group: namespace,
        start_date: 8.days.ago, duration_in_weeks: 2, active: true, automatic: false)
    }

    @iterations = {
      I1: create(:iteration, :skip_future_date_validation,
        group: namespace,
        start_date: 8.days.ago,
        due_date: 5.days.from_now,
        iterations_cadence: @iteration_cadences[:cadence1]),

      I2: create(:iteration, :skip_future_date_validation,
        group: namespace,
        start_date: 6.days.from_now,
        due_date: 19.days.from_now,
        iterations_cadence: @iteration_cadences[:cadence1]),

      I3: create(:iteration, :skip_future_date_validation,
        group: namespace,
        start_date: 20.days.from_now,
        due_date: 33.days.from_now,
        iterations_cadence: @iteration_cadences[:cadence1]),

      I4: create(:iteration, :skip_future_date_validation,
        group: namespace,
        start_date: 34.days.from_now,
        due_date: 47.days.from_now,
        iterations_cadence: @iteration_cadences[:cadence1]),
    }

    create_subgroups
    create_projects

    namespace
  end

  private

  attr_reader :owner, :namespace, :labels, :milestones, :epics, :iterations

  def create_subgroups

    create(:group, name: 'Core Apps', parent: namespace).tap do |group1|

      group1_milestones = {

      }

      create(:board, group: group1, name: 'Development').tap do |board|
        lists = {

        }
      end

      create(:project, :private, :empty_repo,
        import_url: '',
        name: 'Security policy project',
        namespace: group1,
        creator: owner,
        description: 'This project is automatically generated to manage security policies for the project.',
        created_at: '2023-01-18T13:43:41.566Z',
        star_count: 0,
        approvals_before_merge: 0).tap do |project|
        project_labels = {

        }
        project_milestones = {

        }

        create(:board, project: project, name: 'Development').tap do |board|
          lists = {

          }
        end

        mr_203178197 = create(:merge_request, :merged,
          source_project: project,
          title: 'Update scan policies',
          assignees: [owner],
          author: owner,
          source_branch: 'update-policy-1675362373',
          target_branch: 'main',
          labels: [],
          draft: false,
          created_at: '2023-02-02T18:26:13.886Z',

          description: <<~MARKDOWN)

        MARKDOWN

        mr_202950236 = create(:merge_request, :merged,
          source_project: project,
          title: 'Update scan policies',
          assignees: [owner],
          author: owner,
          source_branch: 'update-policy-1675292734',
          target_branch: 'main',
          labels: [],
          draft: false,
          created_at: '2023-02-01T23:05:35.311Z',

          description: <<~MARKDOWN)

        MARKDOWN

        mr_200106191 = create(:merge_request, :merged,
          source_project: project,
          title: 'Update policy.yml',
          assignees: [owner],
          author: owner,
          source_branch: 'juliebyrne-main-patch-77857',
          target_branch: 'main',
          labels: [],
          draft: false,
          created_at: '2023-01-18T15:00:22.102Z',

          description: <<~MARKDOWN)

        MARKDOWN

        mr_200079819 = create(:merge_request, :merged,
          source_project: project,
          title: 'Update scan policies',
          assignees: [owner],
          author: owner,
          source_branch: 'update-policy-1674049423',
          target_branch: 'main',
          labels: [],
          draft: false,
          created_at: '2023-01-18T13:43:43.848Z',

          description: <<~MARKDOWN)

        MARKDOWN
      end
      create(:project, :private, :empty_repo,
        import_url: '',
        name: 'Corporate Website',
        namespace: group1,
        creator: owner,
        description: '',
        created_at: '2022-12-20T19:03:20.233Z',
        star_count: 0,
        approvals_before_merge: 0).tap do |project|

        project_milestones = {

        }

        issue_1 = create(:issue,
          project: project,
          title: 'Story 1',
          assignees: [owner],
          due_date: '',
          weight: 1,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'opened',
          milestone: milestones[:PI_1],
          created_at: 20.days.ago,
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_1,
          weight: 1,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_1,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        issue_2 = create(:issue,
          project: project,
          title: 'Story 2',
          assignees: [owner],
          due_date: '',
          weight: 3,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          closed_at: 2.days.ago,
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_2,
          weight: 3,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_2,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        create(:resource_state_event,
          issue: issue_2,
          created_at: 2.days.ago,
          state: 'closed')

        issue_3 = create(:issue,
          project: project,
          title: 'Story 3',
          assignees: [owner],
          due_date: '',
          weight: 5,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_3,
          weight: 5,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_3,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        issue_4 = create(:issue,
          project: project,
          title: 'Story 4',
          assignees: [owner],
          due_date: '',
          weight: 2,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          milestone: milestones[:PI_1],
          description: ' ')

        create(:resource_weight_event,
          issue: issue_4,
          weight: 2,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_4,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        issue_5 = create(:issue,
          project: project,
          title: 'Story 5',
          assignees: [owner],
          due_date: '',
          weight: 3,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_5,
          weight: 3,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_5,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        issue_6 = create(:issue,
          project: project,
          title: 'Story 6',
          assignees: [owner],
          due_date: '',
          weight: 1,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_6,
          weight: 1,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_6,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        issue_7 = create(:issue,
          project: project,
          title: 'Story 7',
          assignees: [owner],
          due_date: '',
          weight: 5,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'closed',
          epic: @level2_epics[:epic_693723],
          created_at: 20.days.ago,
          closed_at: 1.day.ago,
          milestone: milestones[:PI_1],
          description: ' ')

        create(:resource_weight_event,
          issue: issue_7,
          weight: 5,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_7,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        create(:resource_state_event,
          issue: issue_7,
          created_at: 1.day.ago,
          state: 'closed')

        issue_8 = create(:issue,
          project: project,
          title: 'Story 8',
          assignees: [owner],
          due_date: '',
          weight: 3,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_8,
          weight: 3,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_8,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        issue_9 = create(:issue,
          project: project,
          title: 'Bug 1',
          assignees: [owner],
          due_date: '',
          weight: 1,
          labels: [labels[:type_bug]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_9,
          weight: 1,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_9,
          milestone: milestones[:PI_1],
          created_at: 8.days.ago,
          action: 'add')

        issue_10 = create(:issue,
          project: project,
          title: 'Bug 2',
          assignees: [owner],
          due_date: '',
          weight: 3,
          labels: [labels[:type_bug]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_10,
          weight: 3,
          created_at: 6.days.ago)

        create(:resource_milestone_event,
          issue: issue_10,
          milestone: milestones[:PI_1],
          created_at: 6.days.ago,
          action: 'add')

        issue_11 = create(:issue,
          project: project,
          title: 'Bug 3',
          assignees: [owner],
          due_date: '',
          weight: 2,
          labels: [labels[:type_bug]],
          confidential: false,
          state: 'opened',
          created_at: 20.days.ago,
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_weight_event,
          issue: issue_11,
          weight: 2,
          created_at: 5.days.ago)

        create(:resource_milestone_event,
          issue: issue_11,
          milestone: milestones[:PI_1],
          created_at: 5.days.ago,
          action: 'add')

      end

      create(:project, :private, :empty_repo,
        import_url: '',
        name: 'Online Shopping Site',
        namespace: group1,
        creator: owner,
        description: '',
        created_at: '2022-12-20T19:03:20.233Z',
        star_count: 0,
        approvals_before_merge: 0).tap do |project|

        project_milestones = {

        }

        create(:board, project: project, name: 'Development').tap do |board|
          lists = {

            list_14183720: create(:list,
              board: board,
              label: labels[:status_project_backlog],
              position: 0),

            list_14183719: create(:list,
              board: board,
              label: labels[:status_in_progress],
              position: 1),

          }
        end

        create(:board, project: project, name: 'Workflow').tap do |board|
          lists = {

            list_14183723: create(:list,
              board: board,
              label: labels[:status_project_backlog],
              position: 0),

            list_14183724: create(:list,
              board: board,
              # to do - fix label - use top level Status Labels
              label: labels[:status_in_progress],
              position: 1),

            list_14183725: create(:list,
              board: board,
              # to do - fix label - use top level Status Labels
              label: labels[:status_awaiting_acceptance],
              position: 2),

          }
        end

        create(:board, project: project, name: 'Sprint Planning').tap do |board|
          lists = {

          }
        end

        create(:board, project: project, name: 'Sprint Tracking').tap do |board|
          lists = {

            list_15088795: create(:list,
              board: board,
              label: labels[:status_project_backlog],
              position: 0),

            list_15088794: create(:list,
              board: board,
              label: labels[:status_in_progress],
              position: 1),
            list_sprinttrack3: create(:list,
              board: board,
              label: labels[:status_awaiting_acceptance],
              position: 2),

          }
        end

        issue_123324144 = create(:issue,
          project: project,
          title: 'Improve page load time',
          assignees: [owner],
          due_date: '',
          weight: 3,
          labels: [labels[:status_in_progress], labels[:priority_high], labels[:type_bug]],
          confidential: false,
          state: 'opened',
          milestone: milestones[:PI_1],
          created_at: 1.day.ago,
          epic: epics[:epic_49989],
          iteration: iterations[:I1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_iteration_event,
          issue: issue_123324144,
          iteration: iterations[:I1],
          action: 'add',
          created_at: 1.day.ago)

        create(:resource_weight_event,
          issue: issue_123324144,
          weight: 3,
          created_at: 1.day.ago)

        create(:resource_milestone_event,
          issue: issue_123324144,
          milestone: milestones[:PI_1],
          created_at: 1.day.ago,
          action: 'add')

        issue_121992129 = create(:issue,
          project: project,
          title: 'change image and background',
          assignees: [owner],
          due_date: '',
          weight: 1,
          labels: [labels[:status_in_progress], labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 3.months.ago,
          milestone: milestones[:PI_1],
          iteration: iterations[:I1],
          epic: @level2_epics[:epic_693711],
          description: <<~MARKDOWN)
            Need to update to new logo and bg color that matches
        MARKDOWN

        create(:resource_iteration_event,
          issue: issue_121992129,
          iteration: iterations[:I1],
          action: 'add',
          created_at: 8.days.ago)

        create(:resource_weight_event,
          issue: issue_121992129,
          weight: 1,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_121992129,
          milestone: milestones[:PI_1],
          created_at: 15.days.ago,
          action: 'add')

        issue_121243406 = create(:issue,
          project: project,
          title: 'Add button for 1 click checkout',
          assignees: [owner],
          due_date: '',
          weight: 3,
          labels: [labels[:status_in_progress], labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 3.months.ago,
          closed_at: 3.days.ago,
          iteration: iterations[:I1],
          milestone: milestones[:PI_1],
          epic: @level2_epics[:epic_693711],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_iteration_event,
          issue: issue_121243406,
          iteration: iterations[:I1],
          action: 'add',
          created_at: 8.days.ago)

        create(:resource_weight_event,
          issue: issue_121243406,
          weight: 3,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_121243406,
          milestone: milestones[:PI_1],
          created_at: 15.days.ago,
          action: 'add')

        create(:resource_state_event,
          issue: issue_121243406,
          created_at: 3.days.ago,
          state: 'closed')

        issue_121243354 = create(:issue,
          project: project,
          title: 'Update db schema',
          assignees: [owner],
          due_date: '',
          weight: 5,
          labels: [labels[:status_project_backlog], labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 3.months.ago,
          milestone: milestones[:PI_1],
          iteration: iterations[:I1],
          epic: @level2_epics[:epic_693711],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_iteration_event,
          issue: issue_121243354,
          iteration: iterations[:I1],
          action: 'add',
          created_at: 8.days.ago)

        create(:resource_weight_event,
          issue: issue_121243354,
          weight: 5,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_121243354,
          milestone: milestones[:PI_1],
          created_at: 15.days.ago,
          action: 'add')

        issue_121238144 = create(:issue,
          project: project,
          title: 'Enable code quality scan',
          assignees: [owner],
          due_date: '',
          weight: 2,
          labels: [labels[:type_bug]],
          confidential: false,
          state: 'closed',
          created_at: 4.months.ago,
          closed_at: 5.days.ago,
          iteration: iterations[:I1],
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_iteration_event,
          issue: issue_121238144,
          iteration: iterations[:I1],
          action: 'add',
          created_at: 8.days.ago)

        create(:resource_weight_event,
          issue: issue_121238144,
          weight: 2,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_121238144,
          milestone: milestones[:PI_1],
          created_at: 15.days.ago,
          action: 'add')

        create(:resource_state_event,
          issue: issue_121238144,
          created_at: 5.days.ago,
          state: 'closed')

        issue_120963401 = create(:issue,
          project: project,
          title: 'update app container',
          assignees: [owner],
          due_date: '',
          weight: 3,
          labels: [labels[:status_in_progress], labels[:type_story]],
          confidential: false,
          state: 'opened',
          created_at: 2.months.ago,
          iteration: iterations[:I1],
          milestone: milestones[:PI_1],
          description: <<~MARKDOWN)

        MARKDOWN

        create(:resource_iteration_event,
          issue: issue_120963401,
          iteration: iterations[:I1],
          action: 'add',
          created_at: 8.days.ago)

        create(:resource_weight_event,
          issue: issue_120963401,
          weight: 3,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_120963401,
          milestone: milestones[:PI_1],
          created_at: 15.days.ago,
          action: 'add')

        issue_120711271 = create(:issue,
          project: project,
          title: 'Fix code vulnerability',
          assignees: [owner],
          due_date: '',
          weight: 1,
          labels: [labels[:vulnerability], labels[:type_bug]],
          confidential: false,
          state: 'closed',
          created_at: 2.months.ago,
          closed_at: 5.days.ago,
          milestone: milestones[:PI_1],
          iteration: iterations[:I1],
          epic: @level2_epics[:epic_693711],
          description: <<~MARKDOWN)
            Make a change to bkgd color and image
        MARKDOWN

        create(:resource_iteration_event,
          issue: issue_120711271,
          iteration: iterations[:I1],
          action: 'add',
          created_at: 8.days.ago)

        create(:resource_weight_event,
          issue: issue_120711271,
          weight: 1,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_120711271,
          milestone: milestones[:PI_1],
          created_at: 15.days.ago,
          action: 'add')

        create(:resource_state_event,
          issue: issue_120711271,
          created_at: 5.days.ago,
          state: 'closed')

        issue_120711262 = create(:issue,
          project: project,
          title: 'Make a Big Change',
          assignees: [owner],
          due_date: '',
          weight: 5,
          labels: [labels[:type_story]],
          confidential: false,
          state: 'closed',
          created_at: 4.months.ago,
          closed_at: 4.days.ago,
          milestone: milestones[:PI_1],
          iteration: iterations[:I1],
          epic: @level2_epics[:epic_693711],
          description: <<~MARKDOWN)
            Make a Big Change
        MARKDOWN

        create(:resource_iteration_event,
          issue: issue_120711262,
          iteration: iterations[:I1],
          action: 'add',
          created_at: 8.days.ago)

        create(:resource_weight_event,
          issue: issue_120711262,
          weight: 5,
          created_at: 8.days.ago)

        create(:resource_milestone_event,
          issue: issue_120711262,
          milestone: milestones[:PI_1],
          created_at: 15.days.ago,
          action: 'add')

        create(:resource_state_event,
          issue: issue_120711262,
          created_at: 4.days.ago,
          state: 'closed')

        mr_199900578 = create(:merge_request, :opened,
          source_project: project,
          title: 'Draft: Resolve "change image and background"',
          assignees: [owner],
          author: owner,
          source_branch: '31-change-image-and-background',
          target_branch: 'main',
          labels: [],
          draft: true,
          created_at: '2023-01-17T19:00:24.851Z',
          milestone: project_milestones[:PI_1],
          description: <<~MARKDOWN)
            Fix to update image and background with new look and feel
        MARKDOWN
      end
    end
  end

  def create_projects

    create(:board, group: namespace, name: 'Development').tap do |board|
      lists = {

      }
    end

    create(:board, group: namespace, name: 'Issues by Type').tap do |board|
      lists = {

        list_15090236: create(:list,
          board: board,
          label: labels[:type_bug],
          position: 0),

        list_15090238: create(:list,
          board: board,
          label: labels[:type_story],
          position: 1),
      }
    end
    create(:epic_board,
      group: namespace,
      name: 'Initiatives by Theme',
      labels: [labels[:type_initiative]]).tap do |epic_board|
      epic_lists = {
        epic_list1: create(:epic_list,
          epic_board: epic_board,
          label: labels[:theme_delight_users],
          position: 0),

        epic_list2: create(:epic_list,
          epic_board: epic_board,
          label: labels[:theme_first_class_technology],
          position: 1),

        epic_list3: create(:epic_list,
          epic_board: epic_board,
          label: labels[:theme_increase_customer_spend],
          position: 2),

        epic_list4: create(:epic_list,
          epic_board: epic_board,
          label: labels[:theme_reduce_tech_debt],
          position: 3),
      }
    end
  end
end
